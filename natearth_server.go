package main

import (
	"fmt"
	"strconv"
	"errors"
	"./natearth"
	"encoding/json"
	"net/http"
)

type Entries struct {
	Regions []string `json:"regions"`
}

func geometry(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	enc := json.NewEncoder(w)
	
	// extend with type of output [WKT, WKB, BSON, etc]
	
	// name of region
	var regionName string
	if _, nameOK := params["region"]; nameOK {
		regionName = params["region"][0]
	} else {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 400 Bad Request: %s", errors.New("Parameter 'region' is required"))))
		return
	}
        
	geom, err := natearth.GetGeometry(regionName)
        if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 500: Internal Server Error. %s", err)))
		return
        }
	enc.Encode(geom)
}

func lookAhead(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	enc := json.NewEncoder(w)

	// size of name array
	size := 5
	if _, sizeOK := params["n"]; sizeOK {
		var err error
		size, err = strconv.Atoi(params["n"][0])
        	if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("HTTP/1.1 400 Bad Request: %s", err)))
			return
        	}
	}
	
	// name of region
	var regionName string
	if _, nameOK := params["region"]; nameOK {
		regionName = params["region"][0]
	} else {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 400 Bad Request: %s", errors.New("Parameter 'region' is required"))))
		return
	}


        res, err := natearth.NNames(regionName, size)
        if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("HTTP/1.1 500: Internal Server Error. %s", err)))
		return
        }
	enc.Encode(Entries{res})
}

func main() {
	http.HandleFunc("/lookahead", lookAhead)
	http.HandleFunc("/geometry", geometry)
	http.ListenAndServe("0.0.0.0:3333", nil)
}
